package me.cmder.multiprocess;

import android.content.Context;
import android.util.Log;

import java.security.NoSuchAlgorithmException;
import java.util.Random;

import io.agora.signaling.hq.AgoraHQSigSDK;

/**
 * Created by Yao Ximing on 2018/1/18.
 */

public class Utils {
    public static String randomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int num = random.nextInt(62);
            buf.append(str.charAt(num));
        }
        return buf.toString();
    }

    public static void login(Context context, String appId, AgoraHQSigSDK agoraHQSigSDK, final String tag) {

        String account = Utils.randomString(7);

        appId = context.getString(R.string.agora_app_id);
        agoraHQSigSDK = new AgoraHQSigSDK(context, appId);

        String token = null;
        try {
            token = SignalingToken.getToken(appId, "d02f2a3518434f5b9b6bbd3f94d3065a", account, (int) (System.currentTimeMillis() / 1000) + 100);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        token = "_no_need_token";

        agoraHQSigSDK.login(account, "12345", token, new AgoraHQSigSDK.EventHandler() {
            @Override
            public void onLoginSuccess() {
                Log.e(tag, "success");
            }

            @Override
            public void onError(int error) {
                Log.e(tag, "error:" + error);
            }

            @Override
            public void onChannelMessageReceived(final String channel, final long msgId, final String msg) {
                Log.e(tag, "onChannelMessageReceived:[channel : " + channel + ", msgId : " + msgId + ", msg : " + msg + "]");
            }

            @Override
            public void onMessageReceivedFrom(final String account, final long msgId, final String msg) {
                Log.e(tag, "onMessageReceivedFrom:[account : " + account + ", msgId : " + msgId + ", msg : " + msg + "]");
            }
        });
    }
}
