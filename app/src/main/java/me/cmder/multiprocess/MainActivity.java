package me.cmder.multiprocess;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
    private String[] pac = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 10; j++) {
                String classname = "me.cmder.multiprocess." + pac[i] + ".MyService" + j;
                Log.e("MainActivity", classname);
                Intent myServiceIntent = null;
                try {
                    myServiceIntent = new Intent(this, Class.forName(classname));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                this.startService(myServiceIntent);
            }
        }
    }
}
