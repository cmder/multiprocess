package me.cmder.multiprocess.b;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import io.agora.signaling.hq.AgoraHQSigSDK;
import me.cmder.multiprocess.R;
import me.cmder.multiprocess.Utils;

public class MyService1 extends Service {
    private AgoraHQSigSDK agoraHQSigSDK;

    @Override
    public void onCreate() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String appId = getString(R.string.agora_app_id);
        agoraHQSigSDK = new AgoraHQSigSDK(this, appId);
        Utils.login(this, appId, agoraHQSigSDK, this.getClass().getName());
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
